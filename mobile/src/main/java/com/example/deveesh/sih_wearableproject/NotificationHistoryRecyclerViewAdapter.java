package com.example.deveesh.sih_wearableproject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;


public class NotificationHistoryRecyclerViewAdapter extends RecyclerView.Adapter<NotificationHistoryRecyclerViewAdapter.NotificationListViewHolder> {
    @Override
    public void onViewAttachedToWindow(@NonNull NotificationListViewHolder holder) {
        if(holder instanceof NotificationListViewHolder)
        {
            holder.setIsRecyclable(false);
        }

        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull NotificationListViewHolder holder) {
        if(holder instanceof NotificationListViewHolder)
        {
            holder.setIsRecyclable(true);
        }

        super.onViewDetachedFromWindow(holder);
    }

    public ArrayList<UnknownPersonArrivedModelClass> imageURLs;
    public Context context;
    UnknownPersonArrivedModelClass unknownPersonArrivedModelClass;
    public NotificationHistoryRecyclerViewAdapter(Context ctx, ArrayList<UnknownPersonArrivedModelClass> unknownPersonArrivedModelClass)
    {
        this.imageURLs = unknownPersonArrivedModelClass;
        this.context = ctx;
    }

    @NonNull
    @Override
    public NotificationListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.notification_history_recyclerview_viewholder, viewGroup, false);

        return new NotificationListViewHolder(view, context, imageURLs);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationListViewHolder viewHolder, int i) {
        viewHolder.setIsRecyclable(false);
        if(imageURLs.get(i) != null)
        new DownloadImageTask((ImageView) viewHolder.notificationViewHolder_imageView)
                .execute("http://18.216.164.141:3000/" + imageURLs.get(i).getImageURLs());

        viewHolder.arrivalTime.setText("Arrived: " + imageURLs.get(i).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return imageURLs.size();
    }

    public static class NotificationListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView notificationViewHolder_imageView;
        TextView arrivalTime;
        Context ctx;
        ArrayList<UnknownPersonArrivedModelClass> imageURLs;
        public NotificationListViewHolder(@NonNull View itemView,
                                          Context ctx,ArrayList<UnknownPersonArrivedModelClass> imageURLs) {
            super(itemView);
            this.ctx = ctx;
            this.imageURLs = imageURLs;
            itemView.setOnClickListener(this);
            notificationViewHolder_imageView = (ImageView)itemView.findViewById(R.id.notificationViewHolder_image);
            arrivalTime = (TextView) itemView.findViewById(R.id.unknownPersonArrivalTimeAdapterTextView);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(ctx, imageURLs.get(getAdapterPosition()).getCreatedAt(), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(ctx, FullScreenImageActivity.class);
            intent.putExtra("imgurl", imageURLs.get(getAdapterPosition()) + "," +
                    imageURLs.get(getAdapterPosition()).getCreatedAt());
            ctx.startActivity(intent);
        }
    }

//    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//        ImageView bmImage;
//
//        public DownloadImageTask(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//        protected Bitmap doInBackground(String... urls) {
//            String urldisplay = urls[0];
//            Bitmap mIcon11 = null;
//            try {
//                InputStream in = new java.net.URL(urldisplay).openStream();
//                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("hahahhahaha", e.getMessage());
//                e.printStackTrace();
//            }
//            return mIcon11;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }
}

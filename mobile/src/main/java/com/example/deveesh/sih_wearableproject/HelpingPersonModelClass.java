package com.example.deveesh.sih_wearableproject;

public class HelpingPersonModelClass {

    private int notification_id;
    private String user;

    public int getNotification_id() {
        return notification_id;
    }

    public String getUser() {
        return user;
    }

    public HelpingPersonModelClass(int notification_id, String user) {

        this.notification_id = notification_id;
        this.user = user;
    }
}

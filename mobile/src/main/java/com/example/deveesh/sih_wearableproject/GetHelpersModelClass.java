package com.example.deveesh.sih_wearableproject;

public class GetHelpersModelClass {

//    "id": 1,
//            "notification_id": 197,
//            "user": "naman",
//            "created_at": "2018-11-13T00:09:36.410Z",
//            "updated_at": "2018-11-13T00:09:36.410Z"

    private int id, notification_id;
    private String user;

    public int getId() {
        return id;
    }

    public int getNotification_id() {
        return notification_id;
    }

    public String getUser() {
        return user;
    }

    public GetHelpersModelClass(int id, int notification_id, String user) {

        this.id = id;
        this.notification_id = notification_id;
        this.user = user;
    }
}

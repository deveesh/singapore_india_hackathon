package com.example.deveesh.sih_wearableproject;

import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    EditText mEditTextUsername, mEditTextPassword;
    Spinner loginSpinner;
    int userIndex;
    SharedPreferenceManager sharedPreferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userIndex = 0;
        loginSpinner = (Spinner)findViewById(R.id.login_spinner);
        mEditTextUsername = (EditText)findViewById(R.id.editTextUserName) ;
        mEditTextPassword = (EditText)findViewById(R.id.editTextpassword) ;
        sharedPreferenceManager = new SharedPreferenceManager(this);

        if(sharedPreferenceManager.IsLoggedIn())
        {
            Toast.makeText(this, "saved type= " + sharedPreferenceManager.getType(), Toast.LENGTH_SHORT).show();

            if(sharedPreferenceManager.getType().equals(SharedPreferenceManager.type_STUDENT))
            {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }else
            {
                startActivity(new Intent(LoginActivity.this, AdminActivity.class));

            }
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.login_options, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner

        loginSpinner.setAdapter(adapter);
        loginSpinner.setOnItemSelectedListener(this);

//        Log.d("HAHAHHAAHSFIUAHSF", FirebaseInstanceId.getInstance().getToken());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, "ITEM SELCTED = " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(sharedPreferenceManager.IsLoggedIn())
        {
            Toast.makeText(this, "saved type= " + sharedPreferenceManager.getType(), Toast.LENGTH_SHORT).show();

            if(sharedPreferenceManager.getType().equals(SharedPreferenceManager.type_STUDENT))
            {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }else
            {
                startActivity(new Intent(LoginActivity.this, AdminActivity.class));

            }
        }
    }

    public void login(View view) {
        if(!authenticateCredentials())
        {
            Toast.makeText(this, "Incorrect Credentials", Toast.LENGTH_SHORT).show();
            return;
        }

        sharedPreferenceManager.SetUserIndex(userIndex);

        if(loginSpinner.getSelectedItem().equals("Student"))
        {
            SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(this);

            sharedPreferenceManager.setType(SharedPreferenceManager.type_STUDENT);
            Toast.makeText(this, "1 saved type= " + sharedPreferenceManager.getType(), Toast.LENGTH_SHORT).show();

            sharedPreferenceManager.Login();

            ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            Call<RegisterTokenModelClass> response = apiInterface.setRegisterationToken
                    (new RegisterTokenModelClass(sharedPreferenceManager.GetRegisterationID(), SharedPreferenceManager.type_STUDENT));

            response.enqueue(new Callback<RegisterTokenModelClass>() {
                @Override
                public void onResponse(Call<RegisterTokenModelClass> call, Response<RegisterTokenModelClass> response) {
                    Toast.makeText(LoginActivity.this, "Registered again as student", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<RegisterTokenModelClass> call, Throwable t) {

                }
            });

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }else
        {
            SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(this);

            sharedPreferenceManager.setType(SharedPreferenceManager.type_SECURITY);
            Toast.makeText(this, "2 saved type= " + sharedPreferenceManager.getType(), Toast.LENGTH_SHORT).show();

            sharedPreferenceManager.Login();

            ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            Call<RegisterTokenModelClass> response = apiInterface.setRegisterationToken
                    (new RegisterTokenModelClass(sharedPreferenceManager.GetRegisterationID(), SharedPreferenceManager.type_SECURITY));

            response.enqueue(new Callback<RegisterTokenModelClass>() {
                @Override
                public void onResponse(Call<RegisterTokenModelClass> call, Response<RegisterTokenModelClass> response) {
                    Toast.makeText(LoginActivity.this, "Registered again as security", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<RegisterTokenModelClass> call, Throwable t) {

                }
            });

            startActivity(new Intent(LoginActivity.this, AdminActivity.class));

        }
    }

    private boolean authenticateCredentials() {
        String username = mEditTextUsername.getText().toString();
        String password = mEditTextPassword.getText().toString();

        for(int i=0; i<USERS.userNames.length; i++)
        {
            if(username.equals(USERS.userNames[i]))
            {
                if(password.equals(USERS.passwords[i]))
                {
                    userIndex = i;
                    return true;
                }else
                {
                    return false;
                }
            }
        }
        return false;
    }
}

package com.example.deveesh.sih_wearableproject;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SharedPreferenceManager {

    public String userInfo = "userInfo";
    public String age = "age";
    public String isAgeSet = "isAgeSet";
    public String regis_id = "regis_id";
    public String latitude_key = "latitude_key";
    public String longitude_key = "longitude_key";
    public String type = "type";
    public String isLoggedIn = "isLoggedIn";
    public String userIndex = "userIndex";

    public static final String type_STUDENT = "student";
    public static final String type_SECURITY = "security";

    public void SetUserIndex(int index)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(userIndex, index).commit();
    }

    public int GetUserIndex()
    {
        return sharedPreferences.getInt(userIndex, 0);
    }

    public void Login()
    {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(isLoggedIn, true).commit();
    }

    public boolean IsLoggedIn()
    {
        return sharedPreferences.getBoolean(isLoggedIn, false);
    }

    public String getType() {

        return sharedPreferences.getString(type, type_STUDENT);
    }

    public void setType(String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(type, value).commit();
    }

    private  String pinKey = "PINKEY", pinDefault = "0000",
            isFirstTime = "isFirstTime", contactsKey = "contactsKey";

    SharedPreferences sharedPreferences;

    public SharedPreferenceManager(Context context)
    {
        sharedPreferences = context.getSharedPreferences(userInfo,Context.MODE_PRIVATE);
    }

    public void SetTempLocation(String latitude, String longitude)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(latitude_key, latitude).commit();
        editor.putString(longitude_key, longitude).commit();
    }

    public String[] GetTempLocation()
    {
        String[] location = new String[2];
        location[0] = sharedPreferences.getString(latitude_key, "0.0");
        location[1] = sharedPreferences.getString(longitude_key, "0.0");
        return location;
    }

    public void SetAge(int value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(age, value).commit();
    }

    public void SetRegisterationID(String registerationID)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(regis_id, registerationID).commit();
    }

    public String GetRegisterationID()
    {
        return sharedPreferences.getString(regis_id, "defaultRegisID");
    }

    public int GetAge()
    {
        return sharedPreferences.getInt(age,0);
    }

    public boolean IsAgeSet()
    {
        return sharedPreferences.getBoolean(isAgeSet, false);
    }

    public void SetIsAgeSet()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(isAgeSet, true).commit();
    }

    public String GetPIN()
    {
        return sharedPreferences.getString(pinKey, pinDefault);
    }

    public boolean IsRunningForFirstTime()
    {
        return sharedPreferences.getBoolean(isFirstTime, true);
    }

    public void SetFirstTimeRunToTrue()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(isFirstTime, false).commit();
    }

    public void AddContact(String newContact)
    {
        ArrayList<String> temp = GetContacts();
        temp.add(newContact);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> contactSet = new HashSet<>();
        contactSet.addAll(temp);
        editor.putStringSet(contactsKey, contactSet).commit();
    }

    public ArrayList<String> GetContacts()
    {
        Set<String> set = sharedPreferences.getStringSet(contactsKey, new HashSet<String>());
        ArrayList<String> temp = new ArrayList<>();
        temp.addAll(set);
        return  temp;
    }

    public void UpdatePIN(String newPin)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(pinKey, newPin);
        editor.commit();
    }

}

package com.example.deveesh.sih_wearableproject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpingPersonResponseListItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("notification_id")
    @Expose
    private Integer notificationId;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public HelpingPersonResponseListItem() {
    }

    /**
     *
     * @param updatedAt
     * @param id
     * @param notificationId
     * @param createdAt
     * @param user
     */
    public HelpingPersonResponseListItem(Integer id, Integer notificationId, String user, String createdAt, String updatedAt) {
        super();
        this.id = id;
        this.notificationId = notificationId;
        this.user = user;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}

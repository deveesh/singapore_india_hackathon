package com.example.deveesh.sih_wearableproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        LatLng sydney = new LatLng(-33.852, 151.211);
        LatLng sydney2 = new LatLng(-32.852, 151.211);

        ArrayList<LatLng> locationsOfGH = new ArrayList<>();
        locationsOfGH.add(new LatLng(1.350986, 103.687515));
        locationsOfGH.add(new LatLng(1.351239, 103.688005));
        locationsOfGH.add(new LatLng(1.351574, 103.688474));
        locationsOfGH.add(new LatLng(1.350949, 103.688436));
        googleMap.setIndoorEnabled(true);

        requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 1);
        googleMap.setMyLocationEnabled(true);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
//                            Toast.makeText(MainActivity.this, "OAS " + location.getLatitude(), Toast.LENGTH_SHORT).show();

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude())
                                    , 16.0f));
                        }
                    }
                });



        for(LatLng location : locationsOfGH)
        {
            googleMap.addMarker(new MarkerOptions().position(location).title("Google Home"));
        }

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationsOfGH.get(0)));
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }
}

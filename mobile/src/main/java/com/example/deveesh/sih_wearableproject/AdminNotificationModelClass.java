package com.example.deveesh.sih_wearableproject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdminNotificationModelClass {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("error")
    @Expose
    private Object error;

    /**
     * No args constructor for use in serialization
     *
     */
    public AdminNotificationModelClass() {
    }

    /**
     *
     * @param message
     * @param error
     * @param status
     * @param data
     */
    public AdminNotificationModelClass(List<Datum> data, String message, Integer status, Object error) {
        super();
        this.data = data;
        this.message = message;
        this.status = status;
        this.error = error;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

}

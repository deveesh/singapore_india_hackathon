package com.example.deveesh.sih_wearableproject;

public class RegisterTokenModelClass {
    String registration_id;
    String reg_type;


    public String getType() {
        return reg_type;
    }

    public String getRegistration_id() {
        return registration_id;
    }


    public RegisterTokenModelClass(String registration_id, String type) {
        this.registration_id = registration_id;
        this.reg_type = type;
    }

}

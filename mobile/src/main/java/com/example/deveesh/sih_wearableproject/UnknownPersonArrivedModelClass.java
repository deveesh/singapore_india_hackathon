package com.example.deveesh.sih_wearableproject;

import java.util.ArrayList;

public class UnknownPersonArrivedModelClass {

    private String imageURLs;
    private String createdAt;

    public void setImageURLs(String imageURLs) {
        this.imageURLs = imageURLs;
    }

    public String getImageURLs() {

        return imageURLs;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public String getCreatedAt() {
        return createdAt;
    }

    public UnknownPersonArrivedModelClass(String imageURLs, String createdAt) {

        this.imageURLs = imageURLs;
        this.createdAt = createdAt;
    }
}

package com.example.deveesh.sih_wearableproject;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    String TAG = "TAG";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(this);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        RegisterTokenModelClass registerTokenModelClass = new
                RegisterTokenModelClass(refreshedToken, sharedPreferenceManager.getType());

        retrofit2.Call<RegisterTokenModelClass> result = apiInterface.setRegisterationToken(registerTokenModelClass);

        result.enqueue(new Callback<RegisterTokenModelClass>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterTokenModelClass> call, Response<RegisterTokenModelClass> response) {
                Toast.makeText(MyFirebaseInstanceIDService.this, "Token Registered", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterTokenModelClass> call, Throwable t) {

            }
        });

        sharedPreferenceManager.SetRegisterationID(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(refreshedToken);
    }
}

package com.example.deveesh.sih_wearableproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "Mainactivity";

    Button mButtonHelpMe, mButtonGetNearestGH;

    //Temp variables
    private FusedLocationProviderClient mFusedLocationClient;
    private final int REQUEST_PERMISSION_FINE_LOCATION = 1;
    ApiInterface apiInterface;
    Location currentLocation;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
//                Toast.makeText(MainActivity.this, "location detected..", Toast.LENGTH_SHORT).show();
                currentLocation = location;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Please provide GPS access", Toast.LENGTH_SHORT).show();
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mButtonHelpMe = (Button) findViewById(R.id.button_helpMe);
        mButtonGetNearestGH = (Button) findViewById(R.id.button_getNearestGH);

        mButtonGetNearestGH.setOnClickListener(this);
        mButtonHelpMe.setOnClickListener(this);

        createNotificationChannel();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
    }

    private void ShowNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.this, "CHANNEL_ID")
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentTitle("My notification")
                .setContentText("Much longer text that cannot fit one line...")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Much longer text that cannot fit one line..."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        // notificationId is a unique int for each notification that you must define

        notificationManager.notify(1, mBuilder.build());


    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel_name";
            String description = "channel_description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private boolean hasGps() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_helpMe: {
                Log.d(TAG, "NOTIFYING THE AUTHORITIES!");
                AskAuthoritiesForHelp();
                break;
            }
            case R.id.button_getNearestGH: {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        }
    }

    //    @SuppressLint("MissingPermission")
    private void AskAuthoritiesForHelp() {
        final boolean isAtleastOneLocationSet[] = {false};
        final SharedPreferenceManager preferenceManager = new SharedPreferenceManager(this);

// Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_FINE_LOCATION);
            Toast.makeText(this, "ASOFAUISF", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!hasGps()) {
            Log.d(TAG, "This hardware doesn't have GPS.");
            Toast.makeText(this, "No GPS available", Toast.LENGTH_SHORT).show();
            return;
            // Fall back to functionality that does not use location or
            // warn the user that location function is not available.
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "permission not granted", Toast.LENGTH_SHORT).show();
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
//                            Toast.makeText(MainActivity.this, "Non GPS location detected ", Toast.LENGTH_SHORT).show();
                            preferenceManager.SetTempLocation(location.getLatitude() + "",
                                    location.getLongitude() + "");
                            isAtleastOneLocationSet[0] = true;
                        }
                    }
                });

//        preferenceManager.SetTempLocation(currentLocation.getLatitude() + "",
//                currentLocation.getLongitude() + "");
//        Toast.makeText(this, "currentLocation = " + currentLocation.toString(), Toast.LENGTH_SHORT).show();

        if(currentLocation != null)
        {
            Toast.makeText(this, "GPS location set now", Toast.LENGTH_SHORT).show();
            preferenceManager.SetTempLocation(currentLocation.getLatitude() + "",
                    currentLocation.getLongitude() + "");
        }

        String[] storedLocation = preferenceManager.GetTempLocation();
        GettingHelpModelClass gettingHelpModelClass = new GettingHelpModelClass(storedLocation[0],
                storedLocation[1], preferenceManager.GetRegisterationID().toString());

//        Log.d(TAG, "latitude = " + storedLocation[0] + " longitude = " + storedLocation[1] + " refToken = " + preferenceManager.GetRegisterationID());
//        Toast.makeText(this, "latitude = " + storedLocation[0] + " longitude = " + storedLocation[1] + " refToken = " + preferenceManager.GetRegisterationID(),
//                Toast.LENGTH_SHORT).show();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

//        RegisterTokenModelClass registerTokenModelClass = new RegisterTokenModelClass(preferenceManager.GetRegisterationID());

//        Call<RegisterTokenModelClass> registerRespone = apiInterface.setRegisterationToken(registerTokenModelClass);

//        registerRespone.enqueue(new Callback<RegisterTokenModelClass>() {
//            @Override
//            public void onResponse(Call<RegisterTokenModelClass> call, Response<RegisterTokenModelClass> response) {
//                Toast.makeText(MainActivity.this, "Register again", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<RegisterTokenModelClass> call, Throwable t) {
//
//            }
//        });

        Call<GettingHelpModelClass> response = apiInterface.getHelp(gettingHelpModelClass);

        response.enqueue(new Callback<GettingHelpModelClass>() {
            @Override
            public void onResponse(Call<GettingHelpModelClass> call, Response<GettingHelpModelClass> response) {
                Toast.makeText(MainActivity.this, "notified", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GettingHelpModelClass> call, Throwable t) {
                Toast.makeText(MainActivity.this, "failure23", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(MainActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(MainActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }



    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }
}

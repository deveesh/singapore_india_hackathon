package com.example.deveesh.sih_wearableproject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @POST("send-help")
    Call<GettingHelpModelClass> getHelp(@Body GettingHelpModelClass gettingHelpModelClass);

    @POST("set-registration-token")
    Call<RegisterTokenModelClass> setRegisterationToken(@Body RegisterTokenModelClass registerTokenModelClass);

    @GET("get-notifications")
    Call<AdminNotificationModelClass> getNotificationHistory();

    @POST("set-help-user")
    Call<HelpingPersonModelClass> addHelpingPerson(@Body HelpingPersonModelClass helpingPersonModelClass);

    @POST("get-all-notification-mapping")
    Call<HelpingPersonResponse> getHelingPerson(@Body HelpingPersonModelClass helpingPersonModelClas);


}

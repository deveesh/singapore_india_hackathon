package com.example.deveesh.sih_wearableproject;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView helpingPeopleRecyclerview;
    LinearLayoutManager mLayoutManager;
    ArrayList<String> names;
    Button goToHelp;
    String latitude, longitude, notificationID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        goToHelp = (Button)findViewById(R.id.button_goAndHelp);
        names = new ArrayList<>();

        helpingPeopleRecyclerview = (RecyclerView)findViewById(R.id.helpers_recyclerView);
        mLayoutManager = new LinearLayoutManager(this);

        final HelpingPeopleRecyclerViewAdapter helpingPeopleRecyclerViewAdapter = new HelpingPeopleRecyclerViewAdapter(names);
        helpingPeopleRecyclerview.setAdapter(helpingPeopleRecyclerViewAdapter);
        helpingPeopleRecyclerview.setLayoutManager(mLayoutManager);
        goToHelp.setOnClickListener(this);

        String[] temp = getIntent().getExtras().getString("location").split(",");

        for(int i=0; i<temp.length; i++)
            Toast.makeText(this, "anfoa= " + temp.length, Toast.LENGTH_SHORT).show();

        latitude = temp[0];
        longitude = temp[1];
        notificationID = temp[2];

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

//        Call<List<GetHelpersModelClass>> listOfHelpers = apiInterface.getHelingPerson(
//                new HelpingPersonModelClass(197, "kuchbi"));

        Call<HelpingPersonResponse> listOfHelpers =
                apiInterface.getHelingPerson(new HelpingPersonModelClass(Integer.parseInt(notificationID), "kuchbi"));

        listOfHelpers.enqueue(new Callback<HelpingPersonResponse>() {
            @Override
            public void onResponse(Call<HelpingPersonResponse> call, Response<HelpingPersonResponse> response) {
                Log.d("LOLOLOLAFNKASNF", response.body().getData() + "");
                for(int i=0; i<response.body().getData().size(); i++)
                {
                    names.add(response.body().getData().get(i).getUser());
                    helpingPeopleRecyclerViewAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<HelpingPersonResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.button_goAndHelp:
            {
                ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(this);
                HelpingPersonModelClass helpingPersonModelClass = new HelpingPersonModelClass(Integer.parseInt(notificationID),
                        USERS.userNames[sharedPreferenceManager.GetUserIndex()]);
                Call<HelpingPersonModelClass> response = apiInterface.addHelpingPerson(helpingPersonModelClass);
                response.enqueue(new Callback<HelpingPersonModelClass>() {
                    @Override
                    public void onResponse(Call<HelpingPersonModelClass> call, Response<HelpingPersonModelClass> response) {

                    }

                    @Override
                    public void onFailure(Call<HelpingPersonModelClass> call, Throwable t) {

                    }
                });


                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) == null) {
                    Toast.makeText(this, "Please install Maps", Toast.LENGTH_SHORT).show();
                    return;
                }

                mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mapIntent);
//                PendingIntent pendingIntent = PendingIntent.getActivity(this,0,mapIntent,PendingIntent.FLAG_ONE_SHOT);
                break;
            }
        }
    }
}

package com.example.deveesh.sih_wearableproject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminActivity extends AppCompatActivity {

    RecyclerView notificationHistoryRecyclerView;
    LinearLayoutManager mLayoutManager;
    ArrayList<UnknownPersonArrivedModelClass> unknownPersonArrivedModelClassArrayList;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        notificationHistoryRecyclerView = (RecyclerView)findViewById(R.id.notificationHistoryRecyclerView);
        unknownPersonArrivedModelClassArrayList = new ArrayList<>();
//        notificationHistoryRecyclerView.getRecycledViewPool().setMaxRecycledViews(TYPE_CAROUSEL, 0);
        notificationHistoryRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        notificationHistoryRecyclerView.setLayoutManager(mLayoutManager);

        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AdminNotificationModelClass> result = apiInterface.getNotificationHistory();

        // specify an adapter (see also next example)
        final NotificationHistoryRecyclerViewAdapter mAdapter =
                new NotificationHistoryRecyclerViewAdapter(this, unknownPersonArrivedModelClassArrayList);
        notificationHistoryRecyclerView.setAdapter(mAdapter);
        final int[] tempCounter = {0};
        result.enqueue(new Callback<AdminNotificationModelClass>() {
            @Override
            public void onResponse(Call<AdminNotificationModelClass> call, Response<AdminNotificationModelClass> response) {
                AdminNotificationModelClass output = response.body();
                List<Datum> outputList = output.getData();
                for (Datum dtm : outputList)
                {
                    if((tempCounter[0]++) < 200 && dtm.getData().getImageLink() != null)
                    {
//                        imageURLs.add(dtm.getData().getImageLink());
                        unknownPersonArrivedModelClassArrayList.
                                add(new UnknownPersonArrivedModelClass(dtm.getData().getImageLink(), dtm.getData().getCreated_at()));
//                    Toast.makeText(AdminActivity.this, "added: " + dtm.getData().getImageLink(),
//                            Toast.LENGTH_SHORT).show();
                        mAdapter.notifyDataSetChanged();
                    }else
                    {
//                        break;
                    }

                }
            }

            @Override
            public void onFailure(Call<AdminNotificationModelClass> call, Throwable t) {
                Toast.makeText(AdminActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });


    }


}

package com.example.deveesh.sih_wearableproject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;


public class HelpingPeopleRecyclerViewAdapter extends RecyclerView.Adapter<HelpingPeopleRecyclerViewAdapter.HelpingPeopleViewHolder> {

    public ArrayList<String> personNames;

    public HelpingPeopleRecyclerViewAdapter(ArrayList<String> personNames)
    {
        this.personNames = personNames;
    }

    @NonNull
    @Override
    public HelpingPeopleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.helping_people_recyclerview_viewholder, viewGroup, false);

        return new HelpingPeopleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HelpingPeopleViewHolder viewHolder, int i) {
        viewHolder.name.setText(personNames.get(i));
    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public static class HelpingPeopleViewHolder extends RecyclerView.ViewHolder
    {

        TextView name;

        public HelpingPeopleViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.helpingPersonNameTextView);
        }
    }

}

package com.example.deveesh.sih_wearableproject;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "TAG";

    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // ...
        createNotificationChannel();
        Log.d(TAG, "remoteMessage.getData().size(): " + remoteMessage.getData().size());
        Log.d(TAG, "latitude : " + remoteMessage.getData().get("latitude"));
        Log.d(TAG, "longitude " + remoteMessage.getData().get("longitude"));
        Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//        Log.d(TAG, "Whole response: " + remoteMessage.getD)

//        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");
//        if (mapIntent.resolveActivity(getPackageManager()) != null) {
//            startActivity(mapIntent);
//        }
        // Check if message contains a data payload.

        Integer notificationType = Integer.parseInt(remoteMessage.getData().get("notification_type").toString());

        switch (notificationType) {
            case 0: {
                if (remoteMessage.getData().size() > 0) {
                    String latitude = remoteMessage.getData().get("latitude");
                    String longitude = remoteMessage.getData().get("longitude");
                    String notificationID = remoteMessage.getData().get("notification_id");

//                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
//                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                    mapIntent.setPackage("com.google.android.apps.maps");

                    Intent mapIntent = new Intent(this, HelpActivity.class);
                    mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    mapIntent.putExtra("location", latitude + "," + longitude + "," + notificationID);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,
                            0, mapIntent, PendingIntent.FLAG_ONE_SHOT);

                    Log.d(TAG, "mapIntent.resolveActivity(getPackageManager() = " +
                            mapIntent.resolveActivity(getPackageManager()).toString());
                    if (mapIntent.resolveActivity(getPackageManager()) == null) {
                        Toast.makeText(this, "Please install a map appplication", Toast.LENGTH_SHORT).show();
                        return;
                    }

//            NotificationCompat.Builder  notificationBuilder = new NotificationCompat.Builder(this);
//            notificationBuilder.setContentTitle("Someone is in danger! Click to navigate to them");
//            notificationBuilder.setSmallIcon(R.drawable.helpinghand);
//            notificationBuilder.setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notificationBuilder.build());
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                            .setSmallIcon(R.drawable.helpinghand)
                            .setContentTitle("YOUR HELP NEEDED!")
                            .setContentText("Click to navigate")
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify(1, mBuilder.build());
                }
                break;
            }

            case 1: {
                if (remoteMessage.getData().size() > 0) {
                    Intent mapIntent = new Intent(this, FullScreenImageActivity.class);
//                    String imageUrl = remoteMessage.getData().get("image")
//                    mapIntent.putExtra("imgurl", imageURLs.get(getAdapterPosition()) + "," +
//                            imageURLs.get(getAdapterPosition()).getCreatedAt());

                    mapIntent.putExtra("imgurl", remoteMessage.getData().get("image_link") + "," +
                    remoteMessage.getData().get("created_at"));

//                            mapIntent.putExtra("imgurl", remoteMessage.getData().get("image_link"));
                    mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, mapIntent, PendingIntent.FLAG_ONE_SHOT);

//            NotificationCompat.Builder  notificationBuilder = new NotificationCompat.Builder(this);
//            notificationBuilder.setContentTitle("Someone is in danger! Click to navigate to them");
//            notificationBuilder.setSmallIcon(R.drawable.helpinghand);
//            notificationBuilder.setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notificationBuilder.build());
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                            .setSmallIcon(R.drawable.unknownpersonalert)
                            .setContentTitle("An unknown person is detected")
                            .setContentText("Click to inspect")
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify(1, mBuilder.build());
                }
                break;
            }
            case 2:
            {
                String latitude = remoteMessage.getData().get("latitude");
                String longitude = remoteMessage.getData().get("longitude");

                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) == null) {
                    Toast.makeText(this, "Please install Maps", Toast.LENGTH_SHORT).show();
                    return;
                }

                mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(mapIntent);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0,mapIntent,PendingIntent.FLAG_ONE_SHOT);

//                NotificationCompat.Builder  notificationBuilder = new NotificationCompat.Builder(this);
//                 notificationBuilder.setContentTitle("There is a brawl in the campus. Click to naviagate");
//                 notificationBuilder.setSmallIcon(R.drawable.helpinghand);
//                 notificationBuilder.setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notificationBuilder.build());
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                        .setSmallIcon(R.drawable.unknownpersonalert)
                        .setContentTitle("There is a brawl in the campus. Click to naviagate")
                        .setContentText("Click to navigate")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                // notificationId is a unique int for each notification that you must define
                notificationManager.notify(1, mBuilder.build());
                break;
            }
            case 3:
            {
                String latitude = remoteMessage.getData().get("latitude");
                String longitude = remoteMessage.getData().get("longitude");

                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) == null) {
                    Toast.makeText(this, "Please install Maps", Toast.LENGTH_SHORT).show();
                    return;
                }

                mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(mapIntent);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0,mapIntent,PendingIntent.FLAG_ONE_SHOT);

//                NotificationCompat.Builder  notificationBuilder = new NotificationCompat.Builder(this);
//                 notificationBuilder.setContentTitle("There is a brawl in the campus. Click to naviagate");
//                 notificationBuilder.setSmallIcon(R.drawable.helpinghand);
//                 notificationBuilder.setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notificationBuilder.build());
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                        .setSmallIcon(R.drawable.unknownpersonalert)
                        .setContentTitle("Someone has called for help using voice assistant.")
                        .setContentText("Click to navigate")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                // notificationId is a unique int for each notification that you must define
                notificationManager.notify(1, mBuilder.build());
            }
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel_name";
            String description = "channel_description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}

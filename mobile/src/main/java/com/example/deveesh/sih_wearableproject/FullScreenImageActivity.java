package com.example.deveesh.sih_wearableproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

public class FullScreenImageActivity extends AppCompatActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FullScreenImageActivity.this, AdminActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        String[] imageInfo = getIntent().getExtras().getString("imgurl").split(",");
        String imageUrlToOpen = imageInfo[0];
        String createdAt = imageInfo[1];

        ImageView imageView = (ImageView)findViewById(R.id.fullScreenImage);
        TextView textView = (TextView)findViewById(R.id.personArrivedTextView);
//        textView.setText("Unknown person arrived at " + createdAt);
        new DownloadImageTask(imageView)
                .execute("http://18.216.164.141:3000/" + imageUrlToOpen);
    }

}



package com.example.deveesh.sih_wearableproject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("reg_type")
    @Expose
    private String regType;
    @SerializedName("notification_type")
    @Expose
    private Integer notificationType;
    @SerializedName("image_link")
    @Expose
    private String imageLink;
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param regType
     * @param notificationType
     * @param imageLink
     * @param code
     */
    public Data(String regType, Integer notificationType, String imageLink, String code, String created_at) {
        super();
        this.regType = regType;
        this.notificationType = notificationType;
        this.imageLink = imageLink;
        this.code = code;
        this.created_at = created_at;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {

        return created_at;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;

    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

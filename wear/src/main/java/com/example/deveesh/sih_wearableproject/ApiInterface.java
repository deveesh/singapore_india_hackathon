package com.example.deveesh.sih_wearableproject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {


    @POST("send-help")
    Call<GettingHelpModelClass> getHelp(@Body GettingHelpModelClass gettingHelpModelClass);

    @POST("set-registration-token")
    Call<RegisterTokenModelClass> setRegisterationToken(@Body RegisterTokenModelClass registerTokenModelClass);
}

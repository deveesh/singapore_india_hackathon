package com.example.deveesh.sih_wearableproject;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "TAG";
    public MyFirebaseMessagingService() {
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...
        createNotificationChannel();
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "remoteMessage.getData().size(): " + remoteMessage.getData().size());
        Log.d(TAG, "latitude : " + remoteMessage. getData().get("latitude"));
        Log.d(TAG, "longitude " + remoteMessage.getData().get("longitude"));

//        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");
//        if (mapIntent.resolveActivity(getPackageManager()) != null) {
//            startActivity(mapIntent);
//        }
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
        String latitude = remoteMessage.getData().get("latitude");
        String longitude = remoteMessage.getData().get("longitude");

        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        Log.d(TAG,"mapIntent.resolveActivity(getPackageManager() = " +
                mapIntent.resolveActivity(getPackageManager()).toString());
        if (mapIntent.resolveActivity(getPackageManager()) == null) {
            return;
        }
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,mapIntent,PendingIntent.FLAG_ONE_SHOT);

//            NotificationCompat.Builder  notificationBuilder = new NotificationCompat.Builder(this);
//            notificationBuilder.setContentTitle("Someone is in danger! Click to navigate to them");
//            notificationBuilder.setSmallIcon(R.drawable.helpinghand);
//            notificationBuilder.setContentIntent(pendingIntent);
//
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notificationBuilder.build());

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                    .setSmallIcon(R.drawable.helpinghand)
                    .setContentTitle("YOUR HELP NEEDED!")
                    .setContentText("Click to navigate")
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(pendingIntent);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
            notificationManager.notify(1, mBuilder.build());

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
                Log.d(TAG, "scheduleJobs");
            } else {
                // Handle message within 10 seconds
//                handleNow();
                Log.d(TAG, "handlenow");
            }

        }
    //body-great match
        //title- portu vs denmark
        //datapayload- hi mom
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel_name";
            String description = "channel_description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}

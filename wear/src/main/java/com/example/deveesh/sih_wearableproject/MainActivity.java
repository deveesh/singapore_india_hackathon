package com.example.deveesh.sih_wearableproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.wearable.activity.WearableActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends WearableActivity implements SensorEventListener, View.OnClickListener {

    String TAG = "Mainactivity";

    SensorManager mSensorManager;

    Button mButtonHelpMe, mButtonIsInDanger;

    EditText mEdiText_setHeartBeat;

    Sensor mHeartRateSensor, mStepCountSensor;

    private long maxFastStepCount, currentHeartBeat;
    private float minFastStepInterval;
    private int maxHeartRate;

    private boolean isNotificationSent, isPeronInDanger;
    //Temp variables
    private long lastStepTimeInMiliseconds, currentContinuosFastSteps;
    private FusedLocationProviderClient mFusedLocationClient;
    private final int REQUEST_PERMISSION_FINE_LOCATION = 1;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        minFastStepInterval = 0.2f;
        maxFastStepCount = 20;
        isNotificationSent = false;
        isPeronInDanger = false;
        currentHeartBeat = 0;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        lastStepTimeInMiliseconds = System.currentTimeMillis();
        GetAge();

        mButtonHelpMe = (Button) findViewById(R.id.button_helpMe);
        mButtonIsInDanger = (Button) findViewById(R.id.button_areYouInDanger);
        mEdiText_setHeartBeat = (EditText) findViewById(R.id.ediText_setHeartBeat);
        mButtonHelpMe.setOnClickListener(this);
        mButtonIsInDanger.setOnClickListener(this);
        mEdiText_setHeartBeat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    currentHeartBeat = Integer.parseInt(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        Timer t = new Timer();
//
//        t.scheduleAtFixedRate(
//                new TimerTask()
//                {
//                    public void run()
//                    {
//                        CheckHeartBeat();
//                    }
//                },
//                2000,      // run first occurrence immediatetly
//                2000);

        CheckHeartBeat();

//       createNotificationChannel();
        // Enables Always-on
        setAmbientEnabled();
        initaliseSensor();

//      startActivity(new Intent(MainActivity.this,MapsActivity.class));
    }

    private void GetAge() {
        final SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(MainActivity.this);

        if (sharedPreferenceManager.IsAgeSet() == false) {
            //Ask age..
            final LinearLayout linearLayout = findViewById(R.id.parentLayout_askAge);
//            parentLayout_emergency
            final LinearLayout linearLayout_helpme = findViewById(R.id.parentLayout_emergency);

            Button submitAge = findViewById(R.id.button_submitAge);
            final EditText ageEditText = findViewById(R.id.editText_age);
            linearLayout.setVisibility(View.VISIBLE);
            linearLayout_helpme.setVisibility(View.GONE);
            submitAge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ageEditText.getText().length() > 0) {
                        sharedPreferenceManager.SetAge(Integer.valueOf(ageEditText.getText().toString()));
                        sharedPreferenceManager.SetIsAgeSet();
                        maxHeartRate = 220 - sharedPreferenceManager.GetAge();
                        linearLayout.setVisibility(View.GONE);
                        linearLayout_helpme.setVisibility(View.VISIBLE);
                    }
                }
            });
        } else {
            LinearLayout linearLayout = findViewById(R.id.parentLayout_askAge);
            linearLayout.setVisibility(View.GONE);
            maxHeartRate = 220 - sharedPreferenceManager.GetAge();
        }
    }

    private void ShowNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.this, "CHANNEL_ID")
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentTitle("My notification")
                .setContentText("Much longer text that cannot fit one line...")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Much longer text that cannot fit one line..."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        // notificationId is a unique int for each notification that you must define

        notificationManager.notify(1, mBuilder.build());


    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel_name";
            String description = "channel_description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void initaliseSensor() {
        mSensorManager = ((SensorManager) getSystemService(SENSOR_SERVICE));
        mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        mStepCountSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        mSensorManager.registerListener(this, mStepCountSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mHeartRateSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private boolean hasGps() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    private void CheckHeartBeat() {
//            float currentHeartBeat = event.values[0];
        Log.d(TAG, "currentHearRate = " + currentHeartBeat + " and max = " + maxHeartRate);

        if (currentHeartBeat >= maxHeartRate) {
            //NOTIFY!
            if (!isPeronInDanger) {
                isPeronInDanger = true;
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // Stuff that updates the UI
                        mButtonIsInDanger.setVisibility(View.VISIBLE);
                    }
                });
//                Toast.makeText(this, "Notification will be sent automatically in 5 seconds",
//                        Toast.LENGTH_SHORT).show();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        if (isPeronInDanger) {
                            //NOTIFY
                            Log.d(TAG, "NOTIFYING THE AUTHORITIES!");
                            AskAuthoritiesForHelp();
                            mButtonIsInDanger.setVisibility(View.INVISIBLE);
//                            Toast.makeText(MainActivity.this, "1!", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d(TAG, "NA, YE BHAGRA H");
//                            Toast.makeText(MainActivity.this, "2!", Toast.LENGTH_SHORT).show();
                        }
                        CheckHeartBeat();
                    }
                }, 5000);
            }
        } else {
            isPeronInDanger = false;
            Log.d(TAG, "SB MAST H. CHILL KRO");
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    CheckHeartBeat();
                }
            }, 5000);
//            Toast.makeText(MainActivity.this, "3!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
//            float currentHeartBeat = event.values[0];
            if (currentHeartBeat > maxHeartRate) {
                //NOTIFY!
                if (isPeronInDanger) {
                    mButtonIsInDanger.setVisibility(View.VISIBLE);
                    Toast.makeText(this, "Notification will be sent automatically in 5 seconds",
                            Toast.LENGTH_SHORT).show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            if (isPeronInDanger) {
                                //NOTIFY
                                Toast.makeText(MainActivity.this, "1!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "2!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 5000);
                }
            } else {
                isPeronInDanger = false;
                Toast.makeText(MainActivity.this, "3!", Toast.LENGTH_SHORT).show();
            }
        }

        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            long timeDifferenceBetweenLastStep = System.currentTimeMillis() - lastStepTimeInMiliseconds;
            if (timeDifferenceBetweenLastStep < minFastStepInterval) {
                if (currentContinuosFastSteps >= maxFastStepCount) {
                    //NOTIFY!

                } else {
                    currentContinuosFastSteps++;
                }
            }
            lastStepTimeInMiliseconds = System.currentTimeMillis();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged - accuracy: " + accuracy);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_helpMe: {
                Log.d(TAG, "NOTIFYING THE AUTHORITIES!");
                AskAuthoritiesForHelp();
                mButtonIsInDanger.setVisibility(View.INVISIBLE);
                break;
            }

            case R.id.button_areYouInDanger: {
                isPeronInDanger = false;
            }

        }
    }


//    @SuppressLint("MissingPermission")
//    private void AskAuthoritiesForHelp() {
//        SharedPreferenceManager preferenceManager = new SharedPreferenceManager(this);
////        if (!hasGps()) {
////            Log.d(TAG, "This hardware doesn't have GPS.");
////            Toast.makeText(this, "No GPS available", Toast.LENGTH_SHORT).show();
////            return;
////            // Fall back to functionality that does not use location or
////            // warn the user that location function is not available.
////        }
//        final String[] latitude = new String[1];
//        final String[] longitude = new String[1];
//
//        requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_FINE_LOCATION);
//        mFusedLocationClient.getLastLocation()
//                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
//                    @Override
//                    public void onSuccess(Location location) {
//                        // Got last known location. In some rare situations this can be null.
//                        if (location != null) {
//                            // Logic to handle location object
//                            Toast.makeText(MainActivity.this, "OAS " + location.getLatitude(), Toast.LENGTH_SHORT).show();
//                            latitude[0] = location.getLatitude() + "" ;
//                            longitude[0] = location.getLongitude() + "";
//
//                        }
//                    }
//                });
//
//
//        GettingHelpModelClass gettingHelpModelClass = new GettingHelpModelClass(latitude[0],
//                longitude[0], preferenceManager.GetRegisterationID().toString());
//
//        Log.d(TAG, "latitude = " + latitude[0] + " longitude = " + longitude[0] + " refToken = " + preferenceManager.GetRegisterationID());
//
//        Call<GettingHelpModelClass> response = apiInterface.getHelp(gettingHelpModelClass);
//
//        response.enqueue(new Callback<GettingHelpModelClass>() {
//            @Override
//            public void onResponse(Call<GettingHelpModelClass> call, Response<GettingHelpModelClass> response) {
//                Toast.makeText(MainActivity.this, "notified", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<GettingHelpModelClass> call, Throwable t) {
//                Toast.makeText(MainActivity.this, "failure", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    @SuppressLint("MissingPermission")
    private void AskAuthoritiesForHelp() {
        final SharedPreferenceManager preferenceManager = new SharedPreferenceManager(this);


        requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_FINE_LOCATION);

//        if (!hasGps()) {
//            Log.d(TAG, "This hardware doesn't have GPS.");
//            Toast.makeText(this, "No GPS available", Toast.LENGTH_SHORT).show();
//            return;
//            // Fall back to functionality that does not use location or
//            // warn the user that location function is not available.
//        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            Toast.makeText(MainActivity.this, "OAS", Toast.LENGTH_SHORT).show();
                            preferenceManager.SetTempLocation(location.getLatitude() + "",
                                    location.getLongitude() + "");
                        }
                    }
                });
        String[] storedLocation = preferenceManager.GetTempLocation();
        GettingHelpModelClass gettingHelpModelClass = new GettingHelpModelClass(storedLocation[0],
                storedLocation[1], preferenceManager.GetRegisterationID().toString());

        Log.d(TAG, "latitude = " + storedLocation[0] + " longitude = " + storedLocation[1] + " refToken = " + preferenceManager.GetRegisterationID());
        Toast.makeText(this, "latitude = " + storedLocation[0] + " longitude = " + storedLocation[1] + " refToken = " + preferenceManager.GetRegisterationID(),
                Toast.LENGTH_SHORT).show();

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        RegisterTokenModelClass registerTokenModelClass = new RegisterTokenModelClass(preferenceManager.GetRegisterationID());

        Call<RegisterTokenModelClass> registerRespone = apiInterface.setRegisterationToken(registerTokenModelClass);

        registerRespone.enqueue(new Callback<RegisterTokenModelClass>() {
            @Override
            public void onResponse(Call<RegisterTokenModelClass> call, Response<RegisterTokenModelClass> response) {
                Toast.makeText(MainActivity.this, "Register again", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<RegisterTokenModelClass> call, Throwable t) {

            }
        });
        Call<GettingHelpModelClass> response = apiInterface.getHelp(gettingHelpModelClass);

        response.enqueue(new Callback<GettingHelpModelClass>() {
            @Override
            public void onResponse(Call<GettingHelpModelClass> call, Response<GettingHelpModelClass> response) {
//                Toast.makeText(MainActivity.this, "notified", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GettingHelpModelClass> call, Throwable t) {
                Toast.makeText(MainActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }
}

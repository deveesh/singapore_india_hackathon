package com.example.deveesh.sih_wearableproject;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceManager {

    public String userInfo = "userInfo", age = "age", isAgeSet = "isAgeSet",  regis_id = "regis_id", latitude_key = "latitude_key",
            longitude_key = "longitude_key";
    SharedPreferences sharedPreferences;
    public SharedPreferenceManager(Context context)
    {
        sharedPreferences = context.getSharedPreferences(userInfo,Context.MODE_PRIVATE);
    }

    public void SetTempLocation(String latitude, String longitude)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(latitude_key, latitude).commit();
        editor.putString(longitude_key, longitude).commit();
    }

    public String[] GetTempLocation()
    {
        String[] location = new String[2];
        location[0] = sharedPreferences.getString(latitude_key, "0.0");
        location[1] = sharedPreferences.getString(longitude_key, "0.0");
        return location;
    }

    public void SetAge(int value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(age, value).commit();
    }

    public void SetRegisterationID(String registerationID)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(regis_id, registerationID).commit();
    }

    public String GetRegisterationID()
    {
        return sharedPreferences.getString(regis_id, "defaultRegisID");
    }

    public int GetAge()
    {
        return sharedPreferences.getInt(age,0);
    }

    public boolean IsAgeSet()
    {
        return sharedPreferences.getBoolean(isAgeSet, false);
    }

    public void SetIsAgeSet()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(isAgeSet, true).commit();
    }
}

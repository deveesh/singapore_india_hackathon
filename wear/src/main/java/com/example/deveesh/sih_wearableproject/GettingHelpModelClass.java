package com.example.deveesh.sih_wearableproject;

public class GettingHelpModelClass {


    private String latitude, longitude, registration_id;

    public GettingHelpModelClass(String latitude, String longitude, String registration_id) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.registration_id = registration_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getRegistration_id() {
        return registration_id;
    }
}
